import urllib.request

class Robot:
    def __init__(self, _url):
        self.url = _url
        self.descargado = False

    def retrieve(self):
        if not self.descargado:
            f = urllib.request.urlopen(self.url) #abrimos el documento de la url
            print("Descargando url...")
            self.documento = f.read().decode('utf-8') #leemos y decodificamos = guardamos el documento
            self.descargado = True
            print(f'Url descargada:{self.url}')
        else:
            print(f'La url introducida ya se ha descargado anteriormente: {self.url}')

    def contenido(self):
        self.retrieve()
        return self.documento

    def show(self):
        print(self.contenido())


class Cache:
    def __init__(self):
        self.cache = {} #crecamos un diccionario con las url
        self.encache = False

    def retrieve(self, _url):
        if not self.encache: #_url in self.cache: #si no está la url ya descargada en la cache
            robot = Robot(_url) #nos creamos un objeto robot con esa url para que descargue el documento
            self.cache[_url] = robot#guardamos en el diccionario el
                                                    # contenido del documento con la url como clave
            self.encache = True
        else:
            print('La url introducida ya se encuentra descargada y almacenada en la caché')

    def contenido(self, _url):
        self.retrieve(_url)
        return self.cache[_url].contenido()

    def show(self, _url):
        print('Contenido de la url: ')
        print(self.contenido(_url))

    def show_all(self):
        for url in self.cache:
            print(url)

print('----Ejercicio 16.7: descarga documento web----')
print('')

print('-------------------CLASE ROBOT-----------------')
print('---------------------ROBOT 1-------------------')
robot1 = Robot('https://mail.google.com/mail/u/0/#inbox')
robot1.contenido()
robot1.retrieve()
print('----------------------ROBOT 2------------------')
robot2 = Robot('https://www.aulavirtual.urjc.es')
robot2.contenido()
robot2.retrieve()
robot2.show()
print('')


print('-------------------CLASE CACHÉ-----------------')
print('---------------------CACHÉ 1-------------------')
cache1 = Cache()
cache1.retrieve('http://gsyc.urjc.es/')
cache1.contenido('http://gsyc.urjc.es/')
cache1.show('http://gsyc.urjc.es/')
cache1.show_all()
print('---------------------CACHÉ 2------------------')
cache2 = Cache()
cache2.retrieve('https://labs.etsit.urjc.es/novnc/hafsa/f-l3103-pc02/8004/vnc.html?host=labs.etsit.urjc.es&port=443&path=ws-hafsa-f-l3103-pc02-8004')
cache2.contenido('https://labs.etsit.urjc.es/novnc/hafsa/f-l3103-pc02/8004/vnc.html?host=labs.etsit.urjc.es&port=443&path=ws-hafsa-f-l3103-pc02-8004')
cache2.show('https://labs.etsit.urjc.es/novnc/hafsa/f-l3103-pc02/8004/vnc.html?host=labs.etsit.urjc.es&port=443&path=ws-hafsa-f-l3103-pc02-8004')
cache2.show_all()